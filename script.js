const buttonStop = document.createElement("button");
buttonStop.innerText = "stop";
document.body.prepend(buttonStop);
const buttonStart = document.createElement("button");
buttonStart.innerText = "start";
document.body.prepend(buttonStart);

const images = document.querySelectorAll(".image-to-show");
let currentIndex = 0;
let interval;
for (let i = 0; i < images.length; i++) {
  if (images[currentIndex] === images[i]) {
    images[currentIndex].style.display = "block";
  } else {
    images[i].style.display = "none";
  }
}
function nextImage() {
  images[currentIndex].style.display = "none";
  currentIndex = currentIndex === images.length - 1 ? 0 : ++currentIndex;
  images[currentIndex].style.display = "block";
}
function startShow() {
  interval = setInterval(nextImage, 3000);
}
startShow();
buttonStart.addEventListener("click", startShow);

function stopShow() {
  clearInterval(interval);
}
buttonStop.addEventListener("click", stopShow);
